//HTTP server
package main

import (
	"io"
	"net/http"
)

type (
	Context struct {
		method  string
		handler func(io.Reader) (string, int)
	}
	Router map[string]Context
)

// Handling of http request, and runing business logic
func (route *Router) handleURL(writer http.ResponseWriter, request *http.Request) {

	if context, ok := (*route)[request.RequestURI]; ok {
		if context.method == request.Method {
			answer, code := context.handler(request.Body)
			if code == 0 {
				io.WriteString(writer, answer)
			} else {
				http.Error(writer, answer, code)
			}
		} else {
			http.Error(writer, "Method Not Allowed", 405)
		}
	} else {
		http.Error(writer, "Not Found", 404)
	}
}

func main() {
	//Simple http routing
	router := Router{
		"/balances": Context{
			"GET",
			rpcClient.handleBalace,
		},
		"/transaction": Context{
			"POST",
			rpcClient.handleTransaction,
		},
	}

	http.HandleFunc("/", router.handleURL)
	http.ListenAndServe(":8080", nil)
}
