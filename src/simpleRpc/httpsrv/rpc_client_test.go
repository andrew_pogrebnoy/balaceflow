package main

import (
	"log"
	"testing"
)

var (
	c   *RpcClient
	err error
)

func init() {
	c, err = NewRpcClient("localhost:1313")
	if err != nil {
		log.Fatal(err)
	}
}

func TestGetBalances(t *testing.T) {
	var reply map[int64]int64
	err := c.connection.Call("User.GetAll", &User{}, &reply)
	if err != nil {
		t.Errorf("RPC error: %v", err)
	}

	if len(reply) == 0 {
		t.Errorf("No data in model")
	}
}

func TestSetBalance(t *testing.T) {
	uid := int64(1)

	var userBalance int64
	c.connection.Call("User.GetBallace", &User{uid, 0}, &userBalance)

	var reply int64
	err = c.connection.Call("User.SetBallace", &User{uid, 100}, &reply)
	if reply != userBalance+100 {
		t.Errorf("Error of balance changin: %v", err)
	}

	err = c.connection.Call("User.SetBallace", &User{uid, -1000}, &reply)
	c.connection.Call("User.GetBallace", &User{uid, 0}, &userBalance)

	err = c.connection.Call("User.SetBallace", &User{uid, -1000}, &reply)
	var newBal int64

	c.connection.Call("User.GetBallace", &User{uid, 0}, &newBal)

	if newBal != userBalance {
		t.Errorf("Error! Negative ballace con't be increased %v - %v", newBal, userBalance)
	}

}
