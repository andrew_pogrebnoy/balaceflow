// Part of http server, which provides getting/setting data over rpc
package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net"
	"net/rpc"
	"os"
	"strconv"
)

type (
	User struct {
		Id,
		Balance int64
	}
	RpcClient struct {
		connection *rpc.Client
	}
	RpcServer struct {
		address,
		port string
	}
)

var (
	rpcServer RpcServer
	rpcClient *RpcClient
)

func init() {
	rpcServer = RpcServer{
		"localhost",
		"1313",
	}
	if len(os.Args) > 1 {
		rpcServer.address = os.Args[1]
	}

	// Initializing RPC connection
	var err error
	rpcClient, err = NewRpcClient(rpcServer.address + ":" + rpcServer.port)
	if err != nil {
		log.Fatal(err)
	}
}

func NewRpcClient(dsn string) (*RpcClient, error) {
	connection, err := net.Dial("tcp", dsn)
	if err != nil {
		return nil, err
	}
	return &RpcClient{connection: rpc.NewClient(connection)}, nil
}

// Getting user balances
func (c *RpcClient) handleBalace(_ io.Reader) (string, int) {

	var reply map[int64]int64
	err := c.connection.Call("User.GetAll", &User{}, &reply)
	if err != nil {
		return fmt.Sprintln("RPC error: ", err), 500
	}

	var balances string
	for uid, balance := range reply {
		balances += fmt.Sprintf("\t{\"%v\": %v},\n", uid, balance)

	}

	return fmt.Sprintf("[\n%v\n]", balances[:len(balances)-2]), 0
}

// Changing users balance
func (c *RpcClient) handleTransaction(body io.Reader) (string, int) {

	var u struct {
		UserID,
		Value string
	}

	dec := json.NewDecoder(body)
	err := dec.Decode(&u)

	if err != nil {
		return fmt.Sprintln("Wrong format of request. Unable to parse json: ", err), 400
	}

	var user User
	user.Id, _ = strconv.ParseInt(u.UserID, 0, 64)
	user.Balance, _ = strconv.ParseInt(u.Value, 0, 64)

	var data int64
	err = c.connection.Call("User.SetBallace", &user, &data)
	if err != nil {
		return fmt.Sprintln("Error: ", err), 400
	}

	return fmt.Sprintf("{\"%d\": %d}\n", user.Id, data), 0
}
