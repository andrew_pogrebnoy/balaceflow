//RPC server
package main

import (
	"log"
	"net"
	"net/rpc"
	"simpleRpc/model"
	"sync"
)

type User struct {
	Id,
	Balance int64
}

var mu = &sync.RWMutex{}

// Getting list of all user balances
func (b *User) GetAll(_ *User, reply *map[int64]int64) error {
	mu.RLock()
	defer mu.RUnlock()

	*reply = model.GetBalances()
	return nil
}

func (b *User) GetBallace(user *User, reply *int64) (err error) {
	mu.RLock()
	defer mu.RUnlock()

	*reply, err = model.GetUserBalance(user.Id)
	if err != nil {
		return err
	}
	return nil
}

// Chaneing user balance
func (b *User) SetBallace(user *User, reply *int64) (err error) {
	mu.RLock()
	defer mu.RUnlock()

	*reply, err = model.ChangeUserBalance(user.Id, user.Balance)
	if err != nil {
		return err
	}
	return nil
}

func main() {
	user := new(User)
	rpc.Register(user)

	l, err := net.Listen("tcp", ":1313")
	if err != nil {
		log.Fatal("Unable to start rpc server: ", err.Error())
	}
	rpc.Accept(l)
}
