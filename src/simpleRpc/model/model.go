// It is a separate package just in case of storage changing
package model

import (
	"fmt"
)
type undefUserError int64

var users map[int64]int64

func init() {
	users = map[int64]int64{
		1: 100,
		2: 100,
		3: 100,
		4: 100,
		5: 100,
		6: 100,
		7: 100,
		8: 100,
	}
}

func GetBalances() map[int64]int64 {
	return users
}

func GetUserBalance(uid int64) (int64, error) {
	if value, ok := users[uid]; ok {
		return value, nil
	} else {
		return 0, undefUserError(uid)
	}
}

func ChangeUserBalance(uid int64, factor int64) (int64, error) {
	if _, ok := users[uid]; ok {
		if users[uid] < 0 && factor < 0 {
			return users[uid], fmt.Errorf("Negative ballance can't be increased!\nCurrent balance is %d", users[uid])
		}
		users[uid] += factor
		return users[uid], nil
	} else {
		return 0, undefUserError(uid)
	}
}


func (e undefUserError) Error() string {
	return fmt.Sprintf("Undefined user %d", e)
}
