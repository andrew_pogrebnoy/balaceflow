# GENERAL NOTES #

Application provides functionality of simple account balance flow.
It stores account balance of users, and provides changing and displaying it.

It consists of two parts:

1. HTTP server, witch provides business logic for application which is displaying and changing users balance. It listens port **8080**.

2. RPC server which acts a storage for user balance, which provides storing, updating and lookup of data. Data stores in `Go` type `map`. Listens port **1313**


# RUN #

It is two ways to running the app:

### Docker ###
To run application compiled binaries from Docker images: pull `docker-compose.yml` and run `docker-compose up -d`

Here is two `docker-compose.yml`:

	* In root directory. It is useing images from `docker hub`
	* And in `src/simpleRPC/`. It is builds images useing compiled binaries and Docker files from `src/simpleRPC/rpcsrv/` and `src/simpleRPC/httpsrv/`

### Building from source code ###

	* RPC server `cd src/simpleRPC/rpcsrv/` && `go build`. Then run `./rpcsrv &`
	* HTTP server `cd src/simpleRPC/httpsrv/` && `go build`. Then run `./httpsrv localhost &`


# USING #

**Current balances** 
`GET` request /balances
Returns current balances for all existing users in database. The response is returns in `JSON` 
format.

*Request example*: curl -X GET http://localhost:8080/balances

**Change user’s balance**
`POST` /transaction
The request body should be in `JSON` format with two values: UserID and Value. 
It adds Value to user balance and returns current state

*For example*: `curl -X POST -d '{"UserID":1, "Value":250}' http://localhost:8080/transaction` will increase balance of user `1` on `250`


#TESTS#

Tests are in `src/simpleRpc/httpsrv/rpc_client_test.go`. To run test use `go test` from this directory
